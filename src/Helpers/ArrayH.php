<?php
namespace App\Helpers;
trait ArrayH {

    public static function array_filter_recursive( array $array, callable $callback = null ) {
        $array = is_callable( $callback ) ? array_filter( $array, $callback , ARRAY_FILTER_USE_KEY) : array_filter( $array );
        foreach ( $array as &$value ) {
            if ( is_array( $value ) ) {
                $value = self::array_filter_recursive($value, $callback);
            }
        }
        return $array;
    }
    public static function array_merge_recursive_distinct() {
        $arrays = func_get_args();
        $base = array_shift($arrays);
        if(!is_array($base)) $base = empty($base) ? array() : array($base);
        foreach($arrays as $append) {
            if(!is_array($append)) $append = array($append);
            foreach($append as $key => $value) {
                if(!array_key_exists($key, $base) and !is_numeric($key)) {
                    $base[$key] = $append[$key];
                    continue;
                }
                if(is_array($value) && !array_key_exists($key, $base)) {
                    $base[$key] = $value;
                } elseif(is_array($value) or is_array($base[$key])) {
                    $base[$key] = static::array_merge_recursive_distinct(
                        $base[$key],
                        $append[$key]
                    );
                } elseif(is_numeric($key)) {
                    if(!in_array($value, $base)) $base[] = $value;
                } else {
                    $base[$key] = $value;
                }
            }
        }
        return $base;
    }
}