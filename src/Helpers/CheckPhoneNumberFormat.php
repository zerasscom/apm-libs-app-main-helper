<?php
namespace App\Helpers;

trait CheckPhoneNumberFormat
{

    public static function checkPhoneNumberFormatFrance($phoneNumber)
    {

        $originalNumber = $phoneNumber;
        $returnAr['good'] = false;
        $number = preg_replace('/[^\d]+/', '', $originalNumber);//clear from no number " ", "-" ...
        $returnAr['original'] = $originalNumber;
        $checkAr = ARRAY(
            ARRAY(
                'pattern' => '/^(590?|0?)690([\d]{6})$/',
                'internationalCode' => '590690',
                'localCode' => '0690'
            ),

            ARRAY(
                'pattern' => '/^(590?|0?)696([\d]{6})$/',
                'internationalCode' => '596696',
                'localCode' => '0696'
            ),
            ARRAY(
                'pattern' => '/^(590?|0?)694([\d]{6})$/',
                'internationalCode' => '594694',
                'localCode' => '0694'
            ),
            ARRAY(
                'pattern' => '/^(33?|0?)590([\d]{6})$/',
                'internationalCode' => '33590',
                'localCode' => '0590'
            ),

            ARRAY(
                'pattern' => '/^(337|07|7)([\d]{8})$/',
                'internationalCode' => '337',
                'localCode' => '07'
            ),
            ARRAY(
                'pattern' => '/^(336|06|6)([\d]{2}(?<=[\d]{2})(?<!90)(?<!94)(?<!96)[\d]{6})$/',
                'internationalCode' => '336',
                'localCode' => '06'
            ),

        );

        foreach ($checkAr as $patternInfo) {
            if (preg_match($patternInfo['pattern'], $number, $m)) {
                $returnAr['good'] = true;
                $localNumber = $m[2];

                $returnAr['internationalFullPhoneNumber'] = $patternInfo['internationalCode'] . $localNumber;
                $returnAr['internationalFullPhoneNumberNice'] = '(' . $patternInfo['internationalCode'] . ') ' . $localNumber;
                $returnAr['fullLocalNumber'] = $patternInfo['localCode'] . $localNumber;
                $returnAr['localNumber'] = $localNumber;

                $returnAr['localCode'] = $patternInfo['localCode'];
                $returnAr['internationalCode'] = $patternInfo['internationalCode'];
            }
        }

        return $returnAr;

    }

}