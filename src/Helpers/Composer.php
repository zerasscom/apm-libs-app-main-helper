<?php

namespace App\Helpers;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

trait Composer
{
    public static function postUpdateCmd(Event $event)
    {
        $io = $event->getIO();

        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        #print($vendorPath);
        $fs = new Filesystem;
        $baseDir = dirname($vendorPath);
        if (isset(static::$optionsPaths['path_design'])) {
            $io->write('static::$optionsPaths[\'path_design\'] = ' . static::$optionsPaths['path_design']);
            $requires = $event->getComposer()->getPackage()->getRequires();
            foreach ($requires as $libName => $libObject) {
                if (preg_match('/apm-vlib_([\d]+)\/v([\d]+)/Us', $libName, $m)) {
                    $from = $vendorPath . '/' . $m[0]. '/path_design';
                    $to = $baseDir . static::$optionsPaths['path_design'] . '/' . $m[0];
                    if (file_exists($from)) {

                        // Check the renaming of file for direct moving (file-to-file)
                        $isRenameFile = substr($to, -1) != '/' && !is_dir($from);
                        if (file_exists($to) && !is_dir($to) && !$isRenameFile) {
                            throw new \InvalidArgumentException('Destination directory is not a directory.');
                        }
                        try {
                            if ($isRenameFile) {
                                $fs->mkdir(dirname($to));
                            } else {
                                $fs->mkdir($to);
                            }
                        } catch (IOException $e) {
                            throw new \InvalidArgumentException(sprintf('<error>Could not create directory %s.</error>', $to));
                        }
                        if (false === file_exists($from)) {
                            throw new \InvalidArgumentException(sprintf('<error>Source directory or file "%s" does not exist.</error>', $from));
                        }
                        if (is_dir($from)) {
                            $finder = new Finder;
                            $finder->files()->ignoreDotFiles(false)->in($from);
                            foreach ($finder as $file) {
                                $dest = sprintf('%s/%s', $to, $file->getRelativePathname());
                                try {
                                    $fs->copy($file, $dest);
                                } catch (IOException $e) {
                                    throw new \InvalidArgumentException(sprintf('<error>Could not copy %s</error>', $file->getBaseName()));
                                }
                            }
                        } else {
                            try {
                                if ($isRenameFile) {
                                    $fs->copy($from, $to);
                                } else {
                                    $fs->copy($from, $to.'/'.basename($from));
                                }
                            } catch (IOException $e) {
                                throw new \InvalidArgumentException(sprintf('<error>Could not copy %s</error>', $from));
                            }
                        }
                        $io->write(sprintf('Copied file(s) from <comment>%s</comment> to <comment>%s</comment>.', $m[0]. '/path_design', static::$optionsPaths['path_design'] . '/' . $m[0]));
                    }
                }

            }

        } else {
            $io->write('static::$optionsPaths[\'path_design\'] not founded');
        }
    }
}