<?php
namespace App\Helpers;

use Jenssegers\Agent\Agent;

trait DeviceParams
{
    public static function getDeviceOS($userAgent) {
        $oses = array (
            'iPhone' => '(iPhone)',
            'Windows 3.11' => 'Win16',
            'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
            'Windows 98' => '(Windows 98)|(Win98)',
            'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
            'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
            'Windows 2003' => '(Windows NT 5.2)',
            'Windows Vista' => '(Windows NT 6.0)|(Windows Vista)',
            'Windows 7' => '(Windows NT 6.1)|(Windows 7)',
            'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
            'Windows ME' => 'Windows ME',
            'Open BSD'=>'OpenBSD',
            'Sun OS'=>'SunOS',
            'Linux'=>'(Linux)|(X11)',
            'Safari' => '(Safari)',
            'Macintosh'=>'(Mac_PowerPC)|(Macintosh)',
            'QNX'=>'QNX',
            'BeOS'=>'BeOS',
            'OS/2'=>'OS\\\/2',
            'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp/cat)|(msnbot)|(ia_archiver)'
        );
        foreach($oses as $os=>$pattern){
            if(preg_match("/$pattern/i", $userAgent)) {
                return $os;
            }
        }
        return 'Unknown';
    }

    public static function getDeviceParams(){

        $agent = new Agent();

        if($agent->isMobile()) {
            $device = 'Mobile';
        }
        elseif($agent->isTablet()){
            $device = 'Table';
        }
        elseif($agent->isDesktop()){
            $device = 'Desktop';
        }

        $arrayParams = [
            'user_agent' => $agent->browser() .' '. $agent->version($agent->browser()),
            'ip' => $_SERVER['REMOTE_ADDR'],
//            'device_type' => $agent->device(),
            'device_type' => $device,
//            'operation_system' => DeviceParams::getOS($_SERVER['HTTP_USER_AGENT']),
            'operation_system' => $agent->platform(),
        ];
//dump($device);
        return $arrayParams;

    }

}