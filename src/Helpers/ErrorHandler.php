<?php
namespace App\Helpers;
trait ErrorHandler{
    public static function fatal_handler() {
        $errfile = "unknown file";
        $errstr  = "shutdown";
        $errno   = E_CORE_ERROR;
        $errline = 0;

        $error = error_get_last();

        if( $error !== NULL) {
            $errno   = $error["type"];
            $errfile = $error["file"];
            $errline = $error["line"];
            $errstr  = $error["message"];

            \App\Helpers\ResultReturn::showError($error);
        }
    }
    public static function myErrorHandler($errno, $errstr, $errfile, $errline, $errcontext)
    {
        \App\Helpers\ResultReturn::showError($errno, [$errstr, $errfile, $errline, $errcontext]);
    }
}