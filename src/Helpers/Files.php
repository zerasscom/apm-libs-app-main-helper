<?php
namespace App\Helpers;
trait Files{
    public static function copyDir($source, $distanation) {
        $dir = opendir($source);
        @mkdir($distanation);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($source . '/' . $file) ) {
                    self::copyDir($source . '/' . $file,$distanation . '/' . $file);
                }
                else {
                    copy($source . '/' . $file,$distanation . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}