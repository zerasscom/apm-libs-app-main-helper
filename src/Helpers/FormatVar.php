<?php
namespace App\Helpers;
trait FormatVar {
    public static function mc($value, $maxChars = 0) {
        if ((int)$maxChars > 0) {
            return substr($value, 0, (int)$maxChars);
        } else {
            return $value;
        }
    }
    public static function t($value, $maxChars = 'm') {
        //table_format_return
        return static::mc(
            strtolower(
                trim(
                    preg_replace(
                        '/_+/',
                        '_',
                        preg_replace(
                            '/[^a-zA-Z\d\_]+/',
                            '_',
                            preg_replace(
                                '/([A-Z|А-Я]{1})/u',
                                '_$1',
                                $value
                            ))
                    ),
                    '_')
            ),
            $maxChars);
    }
    public static function v($value, $maxChars = 'm') {
        //varFormatReturn
        return static::mc(preg_replace_callback('![\_]+([\w]{1})!', function ($m){ return strtoupper($m[1]);}, static::t($value)), $maxChars);
    }
    public static function c($value, $maxChars = 'm') {
        //ClassFormatReturn
        return static::mc(ucfirst(static::v($value)), $maxChars);
    }
    public static function ns($nameSpace, $className = '') {
        return static::mc(
            '\\' . ucfirst(
                preg_replace_callback(
                    '![\_]+([\w]{1})!',
                    function ($m){ return '\\' . strtoupper($m[1]);}, static::t($nameSpace)
                )
            ) . (!empty($className)?'\\' . static::c($className):''),
            'm');
    }
    public static function sc($value, $chars = '.', $maxChars = 'm') {
        //var.format.return
        return static::scc($value, $chars,  $maxChars);
    }
    public static function scc($value, $toChars,  $maxChars = 'm', $fromChars = '_') {
        //var.format.return
        return static::mc(preg_replace('/' . preg_quote($fromChars) . '/', $toChars, static::t($value)), $maxChars);
    }
    public static function u($value, $maxChars = 'm') {
        //var-format-return
        return static::sc($value, '-', $maxChars);
    }
    public static function s($value, $maxChars = 'm') {
        //space format return
        return static::sc($value, ' ', $maxChars);
    }
    public static function bs($value, $maxChars = 'm') {
        //Big Space Format Return
        return static::mc(ucfirst(preg_replace_callback('![\_]+([\w]{1})!', function ($m){ return strtoupper($m[1]);}, static::t($value))), $maxChars);
    }

    public static function urlClear($url) {
        return preg_replace('/[\/]+/', '/', $url);
    }

    public static function secondsToTime($seconds) {
        $s = (int)$seconds;
        return sprintf('%02d:%02d:%02d',  $s/3600, $s/60%60, $s%60);
    }


    public static function cVar($varName, $arg, $toType = 'integer', $compulsory = true, $canBeEmpty = true, $debugBacktraceOptions = DEBUG_BACKTRACE_IGNORE_ARGS) {
        if ($compulsory && empty($arg)) {
            $errorText = 'No Arguments Posted! LINE:' . __LINE__;
        } elseif (!(ISSET($arg[$varName]))) {
            if ($compulsory) {
                $errorText = 'VAR NOT ISSET <b>' . $varName . '</b> (' . $toType . ') LINE:' . __LINE__;
            } else return null;
        } else {
            if ($toType == 'integer') {
                if (!$canBeEmpty) if (empty((int)$arg[$varName])) {
                    $errorText = 'empty INTEGER var <b>' . $varName . '</b>' . $toType . '! LINE:' . __LINE__;
                }
                if ($canBeEmpty) {
                    if ($arg[$varName] === '' || $arg[$varName] === null) {
                        return '';
                    } else {
                        return (int)$arg[$varName];
                    }
                } else {
                    return (int)$arg[$varName];
                }
            }elseif (gettype($arg[$varName]) == $toType ||
                ($toType == 'string' && gettype($arg[$varName]) == 'integer')
            ){
                if ((!($canBeEmpty))) if ($arg[$varName] === null) {
                    $errorText = 'Var is empty <b>' . $varName . '</b>' . $toType . '! LINE:' . __LINE__;
                }
                return $arg[$varName];
            } else {
                if ($toType == 'array') {
                    if (empty($arg[$varName])) {
                        return ARRAY();
                    }
                }

                $errorText = 'Different type of var! <b>' . $varName . '</b> is ' . gettype($arg[$varName]) . ' but must be ' . $toType . '! LINE:' . __LINE__ . '<pre>' . print_r($arg[$varName], true);
            }
        }


        $debug_backtrace = debug_backtrace($debugBacktraceOptions);

        \App\Helpers\ResultReturn::showError( $errorText . "\r\n arg: " . print_r($arg, true). "\r\n _GET: " . print_r($_GET,true) . "\r\n_POST" . print_r($_POST,true) . "\r\ndebug_backtrace" . print_r($debug_backtrace,true));
        return false;
    }
}