<?php
namespace App\Helpers;

trait GenerateString
{

    public static function generateString($length = 8){
        $chars = 'abcdefghijklmnopqrstuvwxyz123456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

}