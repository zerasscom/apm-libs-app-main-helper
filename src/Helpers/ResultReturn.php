<?php
namespace App\Helpers;
trait ResultReturn{
    public static function showResult($ar)
    {
        die(json_encode($ar, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }
    public static function showError($errorMsg = 'Something wrong:(', $errorAr = ARRAY() )
    {
        $errorAr = (array)$errorAr;
        $errorAr['error_msg']   = print_r($errorMsg, true);
        $errorAr['error']       = true;
        if (isset($errorAr['json_last_error'])) {
            switch ($errorAr['json_last_error']) {
                case JSON_ERROR_NONE:
                    $errorAr['json_last_error_message'] = ' - Ошибок нет';
                    break;
                case JSON_ERROR_DEPTH:
                    $errorAr['json_last_error_message'] = ' - Достигнута максимальная глубина стека';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $errorAr['json_last_error_message'] = ' - Некорректные разряды или несоответствие режимов';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $errorAr['json_last_error_message'] = ' - Некорректный управляющий символ';
                    break;
                case JSON_ERROR_SYNTAX:
                    $errorAr['json_last_error_message'] = ' - Синтаксическая ошибка, некорректный JSON';
                    break;
                case JSON_ERROR_UTF8:
                    $errorAr['json_last_error_message'] = ' - Некорректные символы UTF-8, возможно неверно закодирован';
                    break;
                default:
                    $errorAr['json_last_error_message'] = ' - Неизвестная ошибка';
                    break;
            }
        }

        self::showResult(array_reverse($errorAr, true));
    }

}