<?php
namespace App\Helpers;
trait Route{
    public static function getURI($apmRemoveRouterBegin = null){

        if(!empty($_SERVER['REDIRECT_URL'])) $url =  parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        elseif(!empty($_SERVER['REQUEST_URI'])) $url =  $_SERVER['REQUEST_URI'];
        elseif(!empty($_SERVER['PATH_INFO'])) $url =  $_SERVER['PATH_INFO'];
        elseif(!empty($_SERVER['QUERY_STRING'])) $url =  $_SERVER['QUERY_STRING'];
        if ($apmRemoveRouterBegin !== null) {
            $url = preg_replace('/^' . preg_quote($apmRemoveRouterBegin) . '/', '', $url);
        }
        return trim($url, '/');
    }
}