<?php
namespace App\Helpers;

trait Youtube
{
    function youtubeGetIdFromUrl($url) {
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|(?:vi|e(?:mbed)?)/|.*[?&]v=|.*[?&]vi=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
        return (ISSET($matches[1]))?$matches[1]:$url;
    }
}