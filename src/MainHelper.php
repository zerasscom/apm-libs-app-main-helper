<?php
namespace App;

class MainHelper {
    public static $optionsPaths = [];

    USE \App\Helpers\ArrayH;
    USE \App\Helpers\ResultReturn;
    USE \App\Helpers\ErrorHandler;

    USE \App\Helpers\FormatVar;
    USE \App\Helpers\Debug;
    USE \App\Helpers\Route;

    USE \App\Helpers\SmartPassword;
    USE \App\Helpers\Secure;

    USE \App\Helpers\Translit;

    USE \App\Helpers\CheckPhoneNumberFormat;
    USE \App\Helpers\DeviceParams;
    USE \App\Helpers\GenerateString;
    USE \App\Helpers\Youtube;

    USE \App\Helpers\Composer;
}