<?php

if (! function_exists('cVar')) {
    function cVar() {
        return call_user_func_array(array('\App\Helpers\FormatVar', 'cVar'), func_get_args());
    }
}
